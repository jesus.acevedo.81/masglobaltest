<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<title>Insert title here</title>
	<link rel="stylesheet"	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    
    <script>
    $(document).ready(function(){
      $("#submitbtn").click(function(e){
          e.preventDefault();
          
          $.ajax({
              type: "GET",
              dataType:"json",
              url:  "http://localhost:8081/api/employees"+( ($.trim($('#employeeID').val()) == '') ? '' : ( '/'+$("#employeeID").val() ) ),
              success: function(result) {
            	  
            	  var table = '<table class="table"> <thead> <tr> <th>id</th> <th>name</th> <th>contractTypeName</th> <th>roleId</th> <th>roleName</th> <th>roleDescription</th> <th>hourlySalary</th> <th>monthlySalary</th> <th>annualSalary</th> </tr> </thead> <tbody> ';
            	  
            	 jQuery(result).each(function(i, item){
            		  
            		  table += '<tr>';
            		  
            		  table += "<td>" + item.id + "</td>" ;
            		  table += "<td>" + item.name + "</td>" ;
            		  table += "<td>" + item.contractTypeName + "</td>" ;
            		  table += "<td>" + item.roleId + "</td>" ;
            		  table += "<td>" + item.roleName + "</td>" ;
            		  table += "<td>" + item.roleDescription + "</td>" ;
            		  table += "<td>" + item.hourlySalary + "</td>" ;
            		  table += "<td>" + item.monthlySalary + "</td>" ;
            		  table += "<td>" + item.annualSalary + "</td>" ;
            		  
            		  table += '</tr>';
            		  
            		});
            	  
            	  
            	  table += "</tbody></table>";
            	  
            	  $("#data").html(table);
              }
          });
          
          
      });
    });
    </script>
        
</head>
<body>


<form>
  <div class="form-group">
    <label for="employeeID">Employee ID</label>
    <input type="number" class="form-control" id="employeeID" placeholder="Employee ID">
  </div>
  <button type="submit" class="btn btn-primary" id="submitbtn">Get Employees</button>
  
</form>

<br/>

<div id="data" ></div>

</body>
</html>