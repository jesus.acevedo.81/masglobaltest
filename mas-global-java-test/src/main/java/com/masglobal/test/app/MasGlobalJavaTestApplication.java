package com.masglobal.test.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasGlobalJavaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasGlobalJavaTestApplication.class, args);
	}

}
