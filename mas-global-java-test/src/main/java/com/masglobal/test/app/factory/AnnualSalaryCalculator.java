package com.masglobal.test.app.factory;

import com.masglobal.test.app.repository.EmployeeEntity;

public interface AnnualSalaryCalculator {

	Integer calculate(EmployeeEntity employee);
	
}
