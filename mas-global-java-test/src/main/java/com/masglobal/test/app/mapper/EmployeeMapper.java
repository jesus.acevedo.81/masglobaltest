package com.masglobal.test.app.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.masglobal.test.app.dto.EmployeeDTO;
import com.masglobal.test.app.factory.AnnualSalaryConcreteFactory;
import com.masglobal.test.app.factory.AnnualSalaryFactory;
import com.masglobal.test.app.repository.EmployeeEntity;

public class EmployeeMapper {
	
	public static List<EmployeeDTO> entityListToDTOList(List<EmployeeEntity> entityList){
		AnnualSalaryFactory calculator = new AnnualSalaryConcreteFactory();
		
		List<EmployeeDTO> employeeList = entityList.stream()
		    .map(obj -> new EmployeeDTO(obj.getId(), obj.getName(), obj.getContractTypeName(), 
		    		obj.getRoleId(), obj.getRoleName(), obj.getRoleDescription(), 
		    		obj.getHourlySalary(), obj.getMonthlySalary(), 
		    		calculator.getCalculator(obj.getContractTypeName()).calculate(obj)))
		    .collect(Collectors.toList());
		
		return employeeList;
	}

}
