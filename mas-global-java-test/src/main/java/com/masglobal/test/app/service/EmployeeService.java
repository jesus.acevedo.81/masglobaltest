package com.masglobal.test.app.service;

import java.util.List;

import com.masglobal.test.app.dto.EmployeeDTO;

public interface EmployeeService {
	
	/**
	 * 
	 * Method to query all employees
	 * 
	 * Complexity O(n) since is required to iterate the list of employees once to create the list of employee DTOs
	 * 
	 * @return list all employees
	 * @throws ServiceException
	 */
	List<EmployeeDTO> getEmployees() throws ServiceException;
	
	/**
	 * 
	 * Method to query all employees with given employeeId
	 * 
	 * Complexity O(n) since is required to iterate the list of employees once to create the list of employee DTOs
	 * 
	 * @param employeeId
	 * @return list of employees with given employeeId
	 * @throws ServiceException
	 */
	List<EmployeeDTO> getEmployees(Integer employeeId) throws ServiceException;

}
