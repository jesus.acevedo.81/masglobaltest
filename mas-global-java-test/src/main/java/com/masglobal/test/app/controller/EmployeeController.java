package com.masglobal.test.app.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.masglobal.test.app.dto.EmployeeDTO;
import com.masglobal.test.app.service.EmployeeService;


@RestController
@RequestMapping("/api")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	
	@GetMapping(value = "/employees/{id}")
    public ResponseEntity<List<EmployeeDTO>> findById(@PathVariable("id") Integer id) {
		
		List<EmployeeDTO> employees;
		try {
			employees = employeeService.getEmployees(id);
			return new ResponseEntity<>(employees, HttpStatus.OK);
		} 
		catch (Exception e) {
			logger.error("Error finding employees", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
    }
	
	@GetMapping(value = "/employees")
    public ResponseEntity<List<EmployeeDTO>> findAll() {
		
		List<EmployeeDTO> employees;
		try {
			employees = employeeService.getEmployees();
			return new ResponseEntity<>(employees, HttpStatus.OK);
		} 
		catch (Exception e) {
			logger.error("Error finding employees", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
    }

}
