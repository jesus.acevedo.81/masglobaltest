package com.masglobal.test.app.repository;

import java.util.List;

import com.masglobal.test.app.service.ServiceException;

public interface EmployeeRepository {

	List<EmployeeEntity> findAll() throws ServiceException;
	EmployeeEntity findById(Integer employeeId) throws ServiceException;
	
}
