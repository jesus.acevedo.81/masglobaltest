package com.masglobal.test.app.factory;

import com.masglobal.test.app.repository.EmployeeEntity;

public class AnnualSalaryConcreteFactory extends AnnualSalaryFactory {

	@Override
	public AnnualSalaryCalculator getCalculator(String contractTypeName) {
		if(contractTypeName!= null && contractTypeName.equals("HourlySalaryEmployee")) {
			return new HourlySalaryAnnualCalculator();
		}
		else if(contractTypeName!= null && contractTypeName.equals("MonthlySalaryEmployee")) {
			return new MonthlySalaryAnnualCalculator();
		}
		else throw new UnexpectedContractTypeException();
	}

}
