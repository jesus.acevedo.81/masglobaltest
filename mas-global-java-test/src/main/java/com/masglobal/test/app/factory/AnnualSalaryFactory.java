package com.masglobal.test.app.factory;

public abstract class AnnualSalaryFactory {
	
	public abstract AnnualSalaryCalculator getCalculator(String contractTypeName) ;

}
