package com.masglobal.test.app.factory;

import com.masglobal.test.app.repository.EmployeeEntity;

public class HourlySalaryAnnualCalculator implements AnnualSalaryCalculator {
	
	@Override
	public Integer calculate(EmployeeEntity employee) {
		return employee.getHourlySalary()*12*120;
	}

}
