package com.masglobal.test.app.service;

public class ServiceException extends Exception {
	
	public ServiceException (Exception ex) {
		super.initCause(ex);
	}

}
