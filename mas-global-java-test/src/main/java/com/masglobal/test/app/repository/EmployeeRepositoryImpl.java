package com.masglobal.test.app.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.masglobal.test.app.service.ServiceException;

@Service
public class EmployeeRepositoryImpl implements EmployeeRepository {
	
	private static final String EMPLOYEE_SERVICE_URL = "http://masglobaltestapi.azurewebsites.net/api/employees";

	@Override
	public List<EmployeeEntity> findAll() throws ServiceException {
		
		return callEmployeesService();
		
	}
	
	public EmployeeEntity findById(Integer employeeId) throws ServiceException {

		List<EmployeeEntity> employees = callEmployeesService();
		
		Optional<EmployeeEntity> optional = employees.stream()
		                .filter(x -> employeeId.equals(x.getId()))
		                .findFirst();
		
		if(optional.isPresent()) {
			return optional.get();
		}
		else {
			return null;
		}
		
	}

	
	
	private List<EmployeeEntity> callEmployeesService() throws ServiceException{
		try {
			RestTemplate restTemplate = new RestTemplate();
			
			EmployeeEntity[] employees = restTemplate.getForObject(EMPLOYEE_SERVICE_URL, EmployeeEntity[].class);
			
			return Arrays.asList(employees);
		}
		catch(Exception ex) {
			throw new ServiceException(ex);
		}
	}

}
