package com.masglobal.test.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.masglobal.test.app.dto.EmployeeDTO;
import com.masglobal.test.app.mapper.EmployeeMapper;
import com.masglobal.test.app.repository.EmployeeEntity;
import com.masglobal.test.app.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public List<EmployeeDTO> getEmployees() throws ServiceException {
		
		try {
			
			List<EmployeeEntity> employees = null;
			
			//find employees
			employees = employeeRepository.findAll();
			
			//map to dto
			if(employees!=null && !employees.isEmpty()) {
				
				return EmployeeMapper.entityListToDTOList(employees);
				
			}
			else {
				return new ArrayList<EmployeeDTO>();
			}
			
		}
		catch(Exception ex) {
			throw new ServiceException(ex);
		}
		
	}
	
	@Override
	public List<EmployeeDTO> getEmployees(Integer employeeId) throws ServiceException {
		
		try {
			
			List<EmployeeEntity> employees = null;
			
			//find employees
			EmployeeEntity employee = employeeRepository.findById(employeeId);
			if(employee != null) {
				employees = new ArrayList<EmployeeEntity>();
				employees.add(employee);
			}
			
			//map to dto
			if(employees!=null && !employees.isEmpty()) {
				
				return EmployeeMapper.entityListToDTOList(employees);
				
			}
			else {
				return new ArrayList<EmployeeDTO>();
			}
			
		}
		catch(Exception ex) {
			throw new ServiceException(ex);
		}
		
	}
	

}
