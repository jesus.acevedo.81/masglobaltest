package com.masglobal.test.app.factory;

import com.masglobal.test.app.repository.EmployeeEntity;

public class MonthlySalaryAnnualCalculator implements AnnualSalaryCalculator {

	@Override
	public Integer calculate(EmployeeEntity employee) {
		return employee.getMonthlySalary()*12;
	}

}
