package com.masglobal.test.app.dto;

import java.io.Serializable;

public class EmployeeDTO implements Serializable {
	
	private Integer id;
	private String name;
	private String contractTypeName;
	private Integer roleId;
	private String roleName;
	private String roleDescription;
	private Integer hourlySalary;
	private Integer monthlySalary;
	private Integer annualSalary;
	
	public EmployeeDTO(Integer id, String name, String contractTypeName, Integer roleId, String roleName,
			String roleDescription, Integer hourlySalary, Integer monthlySalary, Integer annualSalary) {
		super();
		this.id = id;
		this.name = name;
		this.contractTypeName = contractTypeName;
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleDescription = roleDescription;
		this.hourlySalary = hourlySalary;
		this.monthlySalary = monthlySalary;
		this.annualSalary = annualSalary;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getContractTypeName() {
		return contractTypeName;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public Integer getHourlySalary() {
		return hourlySalary;
	}

	public Integer getMonthlySalary() {
		return monthlySalary;
	}

	public Integer getAnnualSalary() {
		return annualSalary;
	}
	
	
}
