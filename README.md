### mas-global-jav-test

- clone the public repository:
https://gitlab.com/jesus.acevedo.81/masglobaltest.git

- compile:
mvn clean install

- run:
mvn spring-boot:run

- enjoy:
front-end:
http://localhost:8081
API
find all:
http://localhost:8081/api/employees
find by employeeId:
http://localhost:8081/api/employees/{employeeId}